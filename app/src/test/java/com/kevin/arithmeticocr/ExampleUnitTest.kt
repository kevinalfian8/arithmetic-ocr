package com.kevin.arithmeticocr

import android.content.Context
import android.content.SharedPreferences
import com.kevin.arithmeticocr.data.EquationResult
import com.kevin.arithmeticocr.db.EquationDao
import com.kevin.arithmeticocr.repo.MainRepositoryImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Answers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class ExampleUnitTest {

    @Mock
    lateinit var dao: EquationDao

    lateinit var repository: MainRepositoryImpl

    @Before
    fun init() {
        val sharedPrefs = Mockito.mock(SharedPreferences::class.java)
        val context: Context = Mockito.mock(Context::class.java)
        repository = MainRepositoryImpl(dao, context, sharedPrefs)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `insert data to room`() = runBlocking {
        repository.insertEquationFile(EquationResult(1, "12+12", "24")).collect {
            assertEquals(listOf(EquationResult(1, "12+12", "24")), it)
        }
    }

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}