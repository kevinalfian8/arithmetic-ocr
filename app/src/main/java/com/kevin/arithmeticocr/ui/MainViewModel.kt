package com.kevin.arithmeticocr.ui

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Tasks
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.kevin.arithmeticocr.data.EquationResult
import com.kevin.arithmeticocr.repo.MainRepository
import com.kevin.arithmeticocr.util.FILE
import com.skydoves.bindables.BindingViewModel
import com.skydoves.bindables.asBindingProperty
import com.skydoves.bindables.bindingProperty
import com.skydoves.whatif.whatIfNotNull
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.objecthunter.exp4j.ExpressionBuilder
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository
) : BindingViewModel() {

    private val _type: MutableStateFlow<String> = MutableStateFlow(FILE)

    private val _equations: MutableStateFlow<List<EquationResult>> = MutableStateFlow(mutableListOf())

    @get:Bindable
    val type: String? by _type.asBindingProperty(viewModelScope, FILE)

    @get:Bindable
    val equations: List<EquationResult> by _equations.asBindingProperty(viewModelScope, emptyList())


    init {
        getCurrentType()
    }

    fun getCurrentType() {
        viewModelScope.launch {
            repository.getCurrentType()?.let {
                _type.emit(it)
            }
        }
    }

    fun setCurrentType(data: String) {
        viewModelScope.launch {
            repository.setCurrentType(data)
            _type.emit(data)

            getAllEquation()
        }
    }

    fun getAllEquation() {
        viewModelScope.launch {
            when (repository.getCurrentType()) {
                FILE -> {
                    repository.getAllEquationFile().flowOn(Dispatchers.IO).collect {
                        _equations.emit(it)
                    }
                }

                else -> {
                    repository.getAllEquationDB().flowOn(Dispatchers.IO).collect {
                        _equations.emit(it)
                    }
                }
            }
        }
    }

    fun addEquation(data: EquationResult) {
        viewModelScope.launch {
            when (repository.getCurrentType()) {
                FILE -> {
                    repository.insertEquationFile(data).flowOn(Dispatchers.IO).collect {
                        _equations.emit(it)
                    }
                }

                else -> {
                    repository.insertEquationDB(data).flowOn(Dispatchers.IO).collect {
                        _equations.emit(it)
                    }
                }
            }
        }
    }

}