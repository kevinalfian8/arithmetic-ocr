package com.kevin.arithmeticocr.ui

import android.Manifest
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.kevin.arithmeticocr.BuildConfig
import com.kevin.arithmeticocr.R
import com.kevin.arithmeticocr.databinding.ActivityMainBinding
import com.kevin.arithmeticocr.util.DB
import com.kevin.arithmeticocr.util.FILE
import com.kevin.arithmeticocr.util.findEquation
import com.skydoves.bindables.BindingActivity
import com.skydoves.whatif.whatIfNotNull
import com.skydoves.whatif.whatIfNotNullOrEmpty
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.File

@AndroidEntryPoint
class MainActivity : BindingActivity<ActivityMainBinding>(R.layout.activity_main), RadioGroup.OnCheckedChangeListener, OnClickListener {

    private val viewModel by viewModels<MainViewModel>()

    private val adapter: MainItemAdapter by lazy {
        MainItemAdapter()
    }

    val openImagePickerLauncher = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { imageUri: Uri? ->
        imageUri?.let {
            findEquation(
                this@MainActivity,
                it,
                { data -> viewModel.addEquation(data) },
                { message -> Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show() }
            )
        }
    }

    val openCamLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) {
        if (it) {
            findEquation(
                this@MainActivity,
                tempUri,
                { data -> viewModel.addEquation(data) },
                { message -> Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show() }
            )
        }
    }

    val requestImagePermission = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
        val isAllowed = it.all { true }

        if (isAllowed) {
            viewModel.getAllEquation()

            if (BuildConfig.FLAVOR_version.equals("camera"))
                tempUri = initTempUri()
        } else {
            finishAffinity()
        }
    }

    private lateinit var tempUri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requestImagePermission.launch(arrayOf(Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE) )
        } else {
            requestImagePermission.launch(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE) )

        }

        binding {
            vm = viewModel
            adapter = this@MainActivity.adapter
            activity = this@MainActivity
        }
    }

    private fun chooseImage() {
        openImagePickerLauncher.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    private fun pickFromCamera() {
        openCamLauncher.launch(tempUri)
    }

    private fun initTempUri(): Uri {
        val tempImagesDir = File(
            applicationContext.filesDir,
            getString(R.string.temp_images_dir))

        tempImagesDir.mkdir()

        val tempImage = File(
            tempImagesDir,
            getString(R.string.temp_image))

        return FileProvider.getUriForFile(
            applicationContext,
            getString(R.string.authorities),
            tempImage)
    }

    private fun actionAdd() {
        if (BuildConfig.FLAVOR_version.equals("file"))
            chooseImage()
        else
            pickFromCamera()
    }

    override fun onCheckedChanged(radioGroup: RadioGroup?, id: Int) {
        when (id) {
            binding.rbFile.id -> viewModel.setCurrentType(FILE)
            else -> viewModel.setCurrentType(DB)
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.fab_add -> {
                actionAdd()
            }
        }
    }
}