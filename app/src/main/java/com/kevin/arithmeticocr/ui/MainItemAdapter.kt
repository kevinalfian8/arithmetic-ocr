package com.kevin.arithmeticocr.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kevin.arithmeticocr.R
import com.kevin.arithmeticocr.data.EquationResult
import com.kevin.arithmeticocr.databinding.ItemResultBinding
import com.skydoves.bindables.BindingListAdapter
import com.skydoves.bindables.binding

class MainItemAdapter: BindingListAdapter<EquationResult,MainItemAdapter.ViewHolder>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.binding(R.layout.item_result))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: ItemResultBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: EquationResult) {
            binding.equationItem = data
            binding.executePendingBindings()
        }
    }

    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<EquationResult>() {

            override fun areItemsTheSame(oldItem: EquationResult, newItem: EquationResult): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: EquationResult, newItem: EquationResult): Boolean =
                oldItem == newItem
        }
    }
}