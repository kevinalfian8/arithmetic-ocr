package com.kevin.arithmeticocr.repo

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.annotation.VisibleForTesting
import com.google.gson.Gson
import com.kevin.arithmeticocr.data.EquationResult
import com.kevin.arithmeticocr.db.EquationDao
import com.kevin.arithmeticocr.util.FILE
import com.kevin.arithmeticocr.util.TYPE
import com.kevin.arithmeticocr.util.getCipher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.crypto.Cipher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@VisibleForTesting
class MainRepositoryImpl @Inject constructor(
    private val dao: EquationDao,
    private val context: Context,
    private val sharedPreferences: SharedPreferences
) : MainRepository {

    override suspend fun insertEquationDB(data: EquationResult): Flow<List<EquationResult>> = flow {
        dao.insertEquation(data)
        val result = dao.getAllEquationResult()
        emit(result)
    }

    override suspend fun getAllEquationDB(): Flow<List<EquationResult>> = flow {
        val result = dao.getAllEquationResult()
        emit(result)
    }

    override suspend fun getAllEquationFile(): Flow<List<EquationResult>> = flow {
        emit(getEquationFile())
    }

    override suspend fun insertEquationFile(data: EquationResult): Flow<List<EquationResult>> = flow {
        emit(insertToEquationFile(data))
    }

    override fun getCurrentType(): String? =
        sharedPreferences.getString(TYPE, FILE)


    override fun setCurrentType(type: String) {
        sharedPreferences.edit().putString(TYPE, type).apply()
    }

    private fun getEquationFile(): List<EquationResult> {
        try {
            val encrypted: ByteArray
            context.openFileInput("result.txt").use {
                encrypted = it.readBytes()
            }

            if (encrypted.isEmpty()) {
                return emptyList()
            }

            val decrypt = getCipher(Cipher.DECRYPT_MODE).doFinal(encrypted)
            val json = String(decrypt)

            val content = Gson().fromJson(
                json,
                Array<EquationResult>::class.java
            )
            return content.toList()
        } catch (e: Exception) {
            return emptyList()
        }
    }

    private fun insertToEquationFile(data: EquationResult): List<EquationResult> {
        val content = getEquationFile().toMutableList()

        try {
            content.add(data)

            context.openFileOutput("result.txt", Context.MODE_PRIVATE).use {
                val json = Gson().toJson(content)
                val encryptedJson = getCipher(Cipher.ENCRYPT_MODE).doFinal(json.toByteArray())

                it.write(encryptedJson)
                it.close()
            }
        } catch (_: Exception) {

        }

        return content
    }

}