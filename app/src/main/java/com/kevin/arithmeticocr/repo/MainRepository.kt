package com.kevin.arithmeticocr.repo

import android.content.Context
import com.kevin.arithmeticocr.data.EquationResult
import kotlinx.coroutines.flow.Flow

interface MainRepository {

    suspend fun insertEquationDB(data: EquationResult): Flow<List<EquationResult>>
    suspend fun getAllEquationDB(): Flow<List<EquationResult>>

    suspend fun getAllEquationFile(): Flow<List<EquationResult>>
    suspend fun insertEquationFile(data: EquationResult): Flow<List<EquationResult>>

    fun getCurrentType(): String?
    fun setCurrentType(type: String)
}