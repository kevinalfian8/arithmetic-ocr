package com.kevin.arithmeticocr.util

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Tasks
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.kevin.arithmeticocr.data.EquationResult
import com.skydoves.whatif.whatIfNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.objecthunter.exp4j.ExpressionBuilder

fun findEquation(context: Context, imageUri: Uri, onSuccess: (data: EquationResult) -> Unit, onFailure: (message: String) -> Unit) {
    val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

    val image: InputImage

    try {
        image = InputImage.fromFilePath(context, imageUri)

        recognizer.process(image).addOnSuccessListener {
            val value = processTextImage(it)

            value.whatIfNotNull(
                whatIf = {
                    onSuccess(it)
                },
                whatIfNot = {
                    onFailure("Can't find any equation")
                }
            )
        }.addOnFailureListener {
            onFailure("Invalid")
        }


    } catch (e: Exception) {
        onFailure("Invalid")
        Log.e("Error", e.message.toString())
    }

}

private fun processTextImage(texts: Text): EquationResult? {
    for (block in texts.textBlocks) {
        for (line in block.lines) {
            val text = line.text
            text.let {
                try {
                    val e = ExpressionBuilder(it).build()

                    val validate = e.validate()

                    if (validate.isValid) {
                        return EquationResult(equation = it, result = e.evaluate().toString())
                    }
                } catch (e: Exception) {
                    return null
                }
            }
        }
    }

    return null
}